var app = angular.module("myApp", ['ui.router']); 
app.config(function($stateProvider, $urlRouterProvider) {
    
	$urlRouterProvider.otherwise('/index')
	
	$stateProvider
	.state('index' ,{
		url: '/index',
		templateUrl: './view/home.html'
	})
	.state('category' ,{
		url: '/category',
		templateUrl: './view/category.html'
	})
	.state('newssection' ,{
		url: '/newssection/:category',
		templateUrl: './view/newssection.html'
	})
	.state('newsdetails' ,{
		url: '/newsdetails/:newsid',
		templateUrl: './view/news-details.html'
	})
	.state('admin' ,{
		url: '/admin',
		templateUrl: './view/admin.html'
	})
	.state('newslist' ,{
		url: '/newslist',
		templateUrl: './view/newslist.html'
	})
	.state('commentlist' ,{
		url: '/commentlist',
		templateUrl: './view/commentlist.html'
	})
	.state('polling' ,{
		url: '/polling',
		templateUrl: './view/polling.html'
	})
	.state('login' ,{
		url: '/login',
		templateUrl: './view/login.html'
	})
	.state('contact' ,{
		url: '/contact',
		templateUrl: './view/contact.html'
	})
});



/*var app = angular.module("myApp", ["ngRoute","ngSanitize"]);

app.config(function($routeProvider) {
    $routeProvider
    .when("/index", {
        templateUrl : "./view/home.html"
    })
    .when("/admin" , {
    	templateUrl : "./view/admin.html"
    })
    .when("/newsdetails/:newsid", {
    	templateUrl : "./view/news-details.html"
    })
    .when("/newslist", {
    	templateUrl : "./view/newslist.html"
    })
    .when("/category", {
    	templateUrl : "./view/category.html"
    })
    .otherwise({
    	redirectTo: "/index"
    });
    
    
});*/









