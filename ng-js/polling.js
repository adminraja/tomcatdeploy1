app.controller('PollCtrl', ['$scope','$http','formdataService', function($scope,$http, formdataService) {

	
  $scope.savepoll = function(){
	  if($scope.answer1!=null){
		  $scope.anwcount1 = 1;
	  }
	  if($scope.answer2!=null){
		  $scope.anwcount2 = 1;
	  }
	  if($scope.answer3!=null){
		  $scope.anwcount3 = 1;
	  }else{
		  $scope.anwcount3 = 0;
	  }
	 
	  $scope.totanswer = $scope.anwcount1+$scope.anwcount2+$scope.anwcount3;
	  
	  var data = {
			  pollid:0,
			  question:$scope.question,
			  answer1:$scope.answer1,
			  answer2:$scope.answer2,
			  answer3:$scope.answer3,
			  totalanswer:$scope.totanswer,
	  }
	  
	  updateimg(data);
	  
  }	
  
   $scope.sendpolldata = function(data){
      $http.post("./rest/news/polling", data).then(function(response) {
  		 $scope.polldata = response.data;
  	  });
   }
   
   $http.get("./rest/news/pollinglist").then(function(response) {
	   $scope.polllist = response.data;
	   
	   $scope.pollid = $scope.polllist[0].pollid; 
	   $scope.answercount1 = $scope.polllist[0].answercount1;
	   $scope.answercount2 = $scope.polllist[0].answercount2;
	   $scope.answercount3 = $scope.polllist[0].answercount3;
	   
	   $scope.answ1 = function(){
		   count = 0;
		   count += 1;
		   $scope.sum1 = count;
		   var a = Number($scope.sum1 || 0);
	       var b = Number($scope.answercount1 || 0);
	       $scope.answercount2;
	       $scope.answercount3;
	       $scope.answercount1 = a+b;
	   }
	   $scope.answ2 = function(){
		   count = 0;
		   count += 1;
		   $scope.sum2 = count;
		   var a = Number($scope.sum2 || 0);
           var b = Number($scope.answercount2 || 0);
           $scope.answercount1;
           $scope.answercount3;
           $scope.answercount2 = a+b;
	   }
	   $scope.answ3 = function(){
		   count = 0;
		   count += 1;
		   $scope.sum3 = +count;
		   var a = Number($scope.sum3 || 0);
           var b = Number($scope.answercount3 || 0);
           $scope.answercount1;
           $scope.answercount2;
           $scope.answercount3 = a+b;
	   }
	   
	   var pollcount1 = Number($scope.answercount1 || 0);
	   var pollcount2 = Number($scope.answercount2 || 0);
	   var pollcount3 = Number($scope.answercount3 || 0);
	   
	   $scope.totalcount = pollcount1+pollcount2+pollcount3; 
	   $scope.btnview = true;
	   $scope.clkvote =function(){
		   $scope.pollresult = true;
		   var data = {
			   answercount1:$scope.answercount1,	
			   answercount2:$scope.answercount2,
			   answercount3:$scope.answercount3,
			   totalanswer:$scope.totalcount,
			   pollid:$scope.pollid
		   }
		   
		   $http.post("./rest/news/pollingupdate", data).then(function(response){
			  $scope.pollupdate = response.data; 
			  if(response.data!=0){
				  $scope.getpolldata();
				  $scope.btnview = false;
			  }
		   });
	   }
	   
   });
  
   $scope.getpolldata = function(){
	   $http.get("./rest/news/pollinglist").then(function(response) {
		   $scope.pollcalculate = response.data;
		   $scope.a1 = $scope.pollcalculate[0].answercount1;
		   $scope.a2 = $scope.pollcalculate[0].answercount2;
		   $scope.a3 = $scope.pollcalculate[0].answercount3;
		   var acount1 =  Number($scope.a1 || 0);
		   var acount2 =  Number($scope.a2 || 0);
		   var acount3 =  Number($scope.a3 || 0);
		   $scope.totalcounts = acount1+acount2+acount3;
		   $scope.percent1 = ($scope.a1/$scope.totalcounts)*100 +"%";
		   $scope.percent2 = ($scope.a2/$scope.totalcounts)*100 +"%";
		   $scope.percent3 = ($scope.a3/$scope.totalcounts)*100 +"%";
	   });
   }
   
   
  
  /* $('#answ1').click(function(){
	   alert("ok");
	   count += 1;
	   $scope.ans1 = count;
	   consolg.log(count);
   });*/
   
   
  var updateimg = function(data){
		var files = angular.element(document.querySelector('.imgupload'));
		if(files.val()!= 0){
			var formdata = new FormData();
		 	for(var i=0;i<files.length;i++){ 
		 		var fileObj= files[i].files;
		 		formdata.append("files" ,fileObj[0]);   
		    }
		 	
		 	 var xhr = new XMLHttpRequest();    
		 	 xhr.open("POST","./rest/file/upload/pollimg");
		 	xhr.send(formdata);
		    	xhr.onload = function(e) {
		    		if (this.status == 200) {
		    			$scope.tmppath;
		    			var obj = JSON.parse(this.responseText);
		    			
		    			var imgpath = obj[0].path;
		    			
		    			data.pollimage = imgpath;
		    			
		    			if(data.pollid == 0){
		    				$scope.sendpolldata(data);
		    			}else{
		    				//$scope.updatenews(data);
		    			}
	    		    }
		    	};
		}else{
			$scope.sendpolldata(data);
		}
	};
	
	
  $scope.survey = { 'title' : '', 'questions': [ { id: 1, 'title' : '', 'answers' : [{id: 1, 'title' : ''/*, 'correct' : 0*/}, {id: 2, 'title' : ''/*, 'correct' : 0*/}] } ] };
  formdataService.addData($scope.survey);

  $scope.addNewQuestion = function() {
    var newQuestionNo = $scope.survey.questions.length+1;
    $scope.survey.questions.push({ 'id': newQuestionNo, 'title' : '', 'answers' : [{ id: 1, 'title' : '', 'correct' : 0}, {id: 2, 'title' : ''/*, 'correct' : 0*/ }] });
  };
  
  $scope.addNewAnswer = function() {
    var newAnswerNo = this.question.answers.length+1;
    this.question.answers.push({ 'id': newAnswerNo, 'title' : ''/*, 'correct' : 0*/ });
  };
  
  $scope.remove = function(item, items) {
  	items.splice(items.indexOf(item), 1);
  	items.forEach( function (elem) {
	  		elem.id = items.indexOf(elem)+1;
  	});
    formdataService.setStep();
  };

  $scope.uncheckSiblings = function(item, items) {
  	if(item.correct) {
	  	$scope.selected = item;
  	}
  }
  
}]);


// controller for preview
app.controller('FormCtrl', function($scope, formdataService) {

  $scope.preview = false;

  // $scope.step = 0;

  $scope.step = formdataService.getStep();

  $scope.formData = formdataService.getData();

  $scope.togglePreview = function() {

    $scope.preview = ($scope.preview === false) ? true : false;

  }

  $scope.setStep = function(num) {

    // $scope.step += num;
    formdataService.setStep(num);
    $scope.step = formdataService.getStep();
    
  };

});


// service for passing data between builder and preview controllers

app.factory('formdataService', function($http) {

  var formData = {};

  var addData = function(newObj) {

    formData = newObj;

  };

  var getData = function() {

    return formData;

  };

  var step = 0;

  var setStep = function(num) {

    step += num;
    
  };

  var getStep = function() {

    return step;

  };

  return {
    addData: addData,
    getData: getData,
    setStep: setStep,
    getStep: getStep
  }

});