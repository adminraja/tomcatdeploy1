app.controller('contCtrl', function($scope,$http,$stateParams,$filter,commonService) {
	
	//for news details 
	$scope.nid = $stateParams.newsid;
	
	$http.get("./rest/news/contentnews/"+$scope.nid).then(function(response){
		$scope.newscontent = response.data;
		var tx = $(".contentnews").val();
		$scope.newsheader = $scope.newscontent[0].header;
		$scope.newssubheader = $scope.newscontent[0].subheader;
		$scope.newscategory = $scope.newscontent[0].category;
		$scope.datetime = $scope.newscontent[0].dateandtime;
		$scope.newsimage = $scope.newscontent[0].image;
		$scope.newscontent = $scope.newscontent[0].content;
		
		
		$http.get("./rest/news/commentlist/"+$scope.nid).then(function(response){
				$scope.commentview = true;
				$scope.commentview = response.data
				if($scope.commentview.length != 23){
				$scope.cmntlength = $scope.commentview.length;
				}else{
					$scope.cmntlength ="0";
				}
					
				/*$scope.list=[{}];
				$scope.listcmnt = [];
				for(var i=0;i<$scope.commentview.length;i++){
					var cmntdate = $scope.commentview[i].dateandtime;
					var date = new Date(cmntdate);
					$scope.dates = date.toGMTString();
					var datess = $scope.dates.split(' ');
					$scope.datevalue = datess[2] + '/' + datess[1] + '/' + datess[3]+' '+datess[4];
					$scope.list.push({
							'dateandtime':$scope.datevalue
						});
					
				}*/
				
			
		});
		/*var k=$scope.nid;
		var second = parseInt(k);
		var nxtdata = second+1;
		$http.get("./rest/news/contentnews/"+nxtdata).then(function(response){
			$scope.nxtcontent = response.data;
			$scope.nxtid = $scope.nxtcontent[0].newsid;
			$scope.nxtheader = $scope.nxtcontent[0].header;
			$scope.nxtsubheader = $scope.nxtcontent[0].subheader;
			$scope.nxtcategory = $scope.nxtcontent[0].category;
			$scope.nxtimage = $scope.nxtcontent[0].image;
			$scope.nxtcontent = $scope.nxtcontent[0].content;
		});*/
		
		$http.get("./rest/news/activenews").then(function(response){
			$scope.nxtcontent = response.data;
			$scope.nxtid = $scope.nxtcontent[0].newsid;
			$scope.nxtheader = $scope.nxtcontent[0].header;
			$scope.nxtsubheader = $scope.nxtcontent[0].subheader;
			$scope.nxtcategory = $scope.nxtcontent[0].category;
			$scope.nxtimage = $scope.nxtcontent[0].image;
			$scope.nxtdate = $scope.nxtcontent[0].dateandtime;
			$scope.nxtcontent = $scope.nxtcontent[0].content;
		});
		
		
		$scope.savecmnt = function() {
			var data = {
					name:$scope.name,
					message:$scope.message,
					newsid:$scope.nid,
					email:$scope.email
			}
			
			$http.post("./rest/news/comment",data).then(function(response){
				$scope.commentdata = response.data;
				$scope.name="";
				$scope.message="";
    			$scope.email=""
    				
			    $http.get("./rest/news/commentlist/"+$scope.nid).then(function(response){
					$scope.commentview = true;
					$scope.commentview = response.data
					if($scope.commentview.length != 23){
					$scope.cmntlength = $scope.commentview.length;
					}else{
						$scope.cmntlength ="0";
					}
				});
			})
			
			
		}
		
//		$('#example1').datepicker({
//	        format: "dd/mm/yyyy"ne
//	    });  
		
	//	function getcmnt(){
		
		
		//}
	});
	
	$scope.dateFormats = function(value){
		return commonService.newsdataFormat(value);
	};
	$scope.dateFormat = function(value){
		return commonService.dataFormat(value);
	};
	
});